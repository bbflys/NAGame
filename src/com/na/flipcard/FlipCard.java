package com.na.flipcard;

import android.content.Intent;
import android.graphics.Typeface;

import com.na.flipcard.ui.WelcomeUI;
import com.na.flipcard.util.GameConfigEx;
import com.na.game.GameActivity;
import com.na.game.GameEntity;
import com.na.game.GameManager;
import com.na.game.platform.sns.QQConnect;
import com.na.game.platform.sns.SnsManager;
import com.na.game.ui.UI;

public class FlipCard extends GameEntity {

	public FlipCard(GameActivity context) {
		super(context);
	}

	@Override
	public <T> T invoke(int function, Class<T> returnType, Object... params) {
		switch (function) {
			case FUNC_GET_INIT_UI:
				return (T) getInitUI();
			case FUNC_GET_DEFAULT_FONT:
				return (T) getDefaultFont();
			case FUNC_ON_ACTIVITY_RESULT:
				onActivityResult((Integer) params[0], (Integer) params[1], (Intent) params[2]);
				break;
		}
		return null;
	}
	
	private UI getInitUI() {
		return new WelcomeUI();
	}
	
	private Typeface getDefaultFont() {
		return Typeface.createFromAsset(context.getAssets(), GameConfigEx.RES_FONT_DF);
//		return Typeface.MONOSPACE;
	}
	
	private void onActivityResult(int requestCode, int resultCode, Intent data) {
		QQConnect tencent = GameManager.get(SnsManager.class).getQQConnect();
		if (tencent != null) {
			tencent.onActivityResult(requestCode, resultCode, data);
		}
	}

}
