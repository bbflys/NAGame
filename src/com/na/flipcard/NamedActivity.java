package com.na.flipcard;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import cn.bmob.BmobObject;

import com.na.flipcard.util.GameConfigEx;
import com.na.game.GameManager;
import com.na.game.data.DataManager;
import com.na.game.flipcard.R;
import com.na.game.platform.sns.SnsListener;
import com.na.game.platform.sns.SnsManager;
import com.na.game.util.Cache;
import com.na.game.util.GameException;

public class NamedActivity extends Activity {

//	private static final String EXPR = "[\\w\\s-_]+";
	
	public static final String EXTRA_DATA = "named_act_extra";
	private ArrayList<Object> values;
	
	private boolean submitLocked = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.na_named_activity);
		
		values = (ArrayList<Object>) getIntent().getSerializableExtra(EXTRA_DATA);
		
		final EditText nickname = (EditText) findViewById(R.id.na_named_my_nickname);
		Button submit = (Button) findViewById(R.id.na_named_submit);
		submit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (submitLocked) {
					Toast.makeText(NamedActivity.this, "正在提交,请稍候...", Toast.LENGTH_SHORT).show();
				} else {
					submitLocked = true;
					check(nickname.getText().toString());
				}
			}
		});
	}
	
	private void check(String nickname) {
		nickname = nickname.trim();
		if (nickname.length() == 0) {
			Toast.makeText(this, "请输入名字后再提交哦", Toast.LENGTH_LONG).show();
			return;
		}
//		if (!nickname.matches(EXPR)) {
//			Toast.makeText(this, "名字里不能有特殊字符哦", Toast.LENGTH_LONG).show();
//			return;
//		}
		Toast.makeText(this, "正在提交,请稍后...", Toast.LENGTH_SHORT).show();
		checkFromServer(nickname);
	}
	
	private void checkFromServer(final String nickname) {
		new Thread() {
			@Override
			public void run() {
				final SnsManager sns = GameManager.get(SnsManager.class);
				SnsListener listener = new SnsListener() {
					@Override
					public void queryResult(List<BmobObject> result, GameException ex) {
						if (ex != null || (result != null && result.size() > 0)) {
							submitLocked = false;
							NamedActivity.this.runOnUiThread(new Runnable() {
								@Override
								public void run() {
									Toast.makeText(NamedActivity.this, "糟糕," + nickname + "已经被别人抢先注册了,只好换一个名字咯", Toast.LENGTH_LONG).show();
								}
							});
						} else {
							BmobObject obj = (BmobObject) Cache.getCache().get(GameConfigEx.CACHE_BMOB_SCORE);
							if (obj != null) {
								GameManager.get(DataManager.class).put(GameConfigEx.DATA_MY_NICKNAME, nickname);
								values.add(GameConfigEx.BMOB_TABLE_SCORES_COL_NICKNAME);
								values.add(nickname);
								sns.save(obj, GameConfigEx.BMOB_TABLE_SCORES, values.toArray(), this);
							} else {
								submitLocked = false;
							}
						}
					}
					
					@Override
					public void saveCallback(final String msg) {
						NamedActivity.this.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								GameManager.get(DataManager.class).put(GameConfigEx.DATA_SUBMIT_SCORE, Integer.valueOf(127)); // mark
								Toast.makeText(NamedActivity.this, msg == null ? "分数提交成功,您可以到排行榜去查看您的战绩" : msg, Toast.LENGTH_SHORT).show();
								NamedActivity.this.finish();
							}
						});
					}
				};
				sns.query(GameConfigEx.BMOB_TABLE_SCORES, listener, GameConfigEx.BMOB_TABLE_SCORES_COL_NICKNAME, nickname);
			}
		}.start();
	}

}
