package com.na.flipcard;

public interface Effect {
	public boolean isSelected(int gridIndex);
	public boolean isDither();
	public String getScoreTxt();
	public int getMul();
}
