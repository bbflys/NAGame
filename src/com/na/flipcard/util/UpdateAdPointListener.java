package com.na.flipcard.util;

import com.na.game.GameManager;
import com.na.game.data.DataManager;
import com.na.game.platform.ad.AdListener;
import com.na.game.platform.ad.AdPoints;

public class UpdateAdPointListener implements AdListener<AdPoints> {

	@Override
	public void notify(int type, AdPoints param) {
		if (type == TYPE_AD_POINTS) {
			GameManager.get(DataManager.class).put(GameConfigEx.DATA_MY_POINTS, param.point);
		}
	}

}
