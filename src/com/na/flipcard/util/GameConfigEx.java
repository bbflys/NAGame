package com.na.flipcard.util;

import com.na.game.util.GameConfig;

public class GameConfigEx extends GameConfig {
	public static final String RES_FONT_DF = "fonts/df.ttf";
	public static final String RES_BIG_ICON_1 = "1_big_icon.png";
	public static final String RES_BIG_ICON_2 = "2_big_icon.png";
	public static final String RES_BIG_ICON_3 = "3_big_icon.png";
	public static final String RES_BUTTON_BACK_PRESSED = "button_back_down.png";
	public static final String RES_BUTTON_BACK = "button_back_up.png";
	public static final String RES_BUTTON_PRESSED = "button_image_down.png";
	public static final String RES_BUTTON = "button_image.png";
	public static final String RES_BUTTON_PAUSE = "button_pause.png";
	public static final String RES_BUTTON_SELECTED = "button_selected_icon.png";
	public static final String RES_BUTTON_START = "button_start.png";
	public static final String RES_BACKGROUND = "chalkboard.png";
	public static final String RES_COIN = "coin.png";
	public static final String RES_COMBO_TIME_BARS = "combo_time_bars.png";
	public static final String RES_COMBO_TIME_SLOT = "combo_time_slot.png";
	public static final String RES_GAME_BG = "game_bg.png";
	public static final String RES_GAME_PANEL = "game_panel.png";
	public static final String RES_GAME_PANEL2 = "game_panel2.png";
	public static final String RES_LONG_BUTTON_PRESSED = "long_button_down.png";
	public static final String RES_LONG_BUTTON = "long_button.png";
	public static final String RES_TITLE_BAR = "promobar.png";
	public static final String RES_SELECTED = "selected.png";
	public static final String RES_TIME_BARS = "time_bars.png";
	public static final String RES_TIME_SLOT = "time_slot.png";
	public static final String RES_NAME_TIAN = "tian.png";
	public static final String RES_NAME_KA = "ka.png";
	public static final String RES_NAME_PIAN = "pian.png";
	public static final String RES_PLIST_BIG_ICON_1 = "bigicon1.plist";
	public static final String RES_PLIST_BIG_ICON_2 = "bigicon2.plist";
	public static final String RES_PLIST_BIG_ICON_3 = "bigicon3.plist";
	
	public static final String RES_SOUND_CLICK = "sounds/click.ogg";
	public static final String RES_SOUND_CLICK2 = "sounds/click2.wav";
	public static final String RES_SOUND_CLICK_BAN = "sounds/click_ban.ogg";
	public static final String RES_SOUND_BINGO = "sounds/bingo.wav";
	public static final String RES_SOUND_WARN = "sounds/warn.wav";
	public static final String RES_SOUND_START = "sounds/start.wav";
	public static final String RES_SOUND_WIND = "sounds/wind.ogg";
	
	public static final int[] GAME_ENTRY_SIZE = new int[]{4, 5, 5, 6, 6, 6, 7, 7, 8, 9, 10, 12, 15, 17};
	public static final int[] GAME_SCORES = new int[]{10, 29, 29, 39, 39, 39, 50, 50, 62, 75, 90, 113, 145, 180};
	public static final int[] GAME_ENTRY_REMOVE_PRO = new int[]{0, 0, 0, 10, 10, 10, 15, 15, 20, 25, 30, 40, 50, 60};
	
	@Deprecated
	public static final String DATA_FIRST_IN = "first_play_game";
	public static final String DATA_SCORE_HIGHEST_DAY = "score_highest_day";
	public static final String DATA_SCORE_HIGHEST_WEEK = "score_highest_week";
	public static final String DATA_SCORE_HIGHEST_ALL = "score_highest_all";
	public static final String DATA_SAVE_SCORE_DATE = "save_score_date";
	public static final String DATA_MY_POINTS = "na_my_points";
	public static final String DATA_SUBMIT_SCORE = "na_submit_server_score"; // Integer
	public static final String DATA_MY_NICKNAME = "na_my_nickname"; // String
	
	public static final int GAME_TIME_LIMIT = 60000; // 60s
	@Deprecated
	public static final int SCORE_LOSE = 60;
	public static final String GAME_OVER_DESC_FIRST_WIN = "不错嘛，第一次玩都这么厉害，再接再厉啦";
	@Deprecated
	public static final String GAME_OVER_DESC_LOSE = "不要气馁，加油啦";
	public static final String GAME_OVER_DESC_HIGHEST_DAY = "恭喜你啦，创造了今日最高得分记录";
	public static final String GAME_OVER_DESC_HIGHEST_WEEK = "恭喜你啦，创造了本周最高得分记录";
	public static final String GAME_OVER_DESC_HIGHEST_ALL = "恭喜你啦，创造了历史最高得分记录";
	public static final String GAME_OVER_DESC_NORMAL = "继续加油哦";
	
	public static final int COLOR_MENU_FONT = 0xff12a495;
	public static final int COLOR_DEEP_BLUE = 0xff147f7e;
	public static final int COLOR_ORANGE = 0xffff8e00;
	public static final int COLOR_YELLOW = 0xff13aa9b;//0xfff7b822;
	public static final int COLOR_GREEN = 0xff17bfaf;//0xff13aa9b;
	
	public static final String BMOB_TABLE_SCORES = "scores";
	public static final String BMOB_TABLE_SCORES_COL_USERID = "userId";
	public static final String BMOB_TABLE_SCORES_COL_NICKNAME = "nickname";
	public static final String BMOB_TABLE_SCORES_COL_HIGHEST = "highest_score";
	public static final String BMOB_TABLE_SCORES_COL_ADDITIVE = "additive_score";
	public static final String BMOB_TABLE_PROPS_LOG = "props_use_log";
	public static final String BMOB_TABLE_PL_COL_USERID = "userId";
	public static final String BMOB_TABLE_PL_COL_PHONE = "phone";
	public static final String BMOB_TABLE_PL_COL_PN = "propsName";
	public static final String BMOB_TABLE_PL_COL_PA = "propsAmount";
	
	public static final String CACHE_BMOB_SCORE = "score_in_bmob";
	
	public static final int TEXT_SIZE_XLARGE = 70;
	public static final int TEXT_SIZE_MEDIUM = 40;
	public static final int TEXT_SIZE_SMALL = 35;
	
	public static final String WAPS_CONFIG_WHITEUSER = "IS_WHITE_USER";
	public static boolean isWhiteUser = false; // 白名单用户，不显示广告（hiapk、goapk市场使用。必须在waps初始化后，从其服务器获取该参数值）
}
