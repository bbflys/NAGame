package com.na.flipcard.util;

import android.view.MotionEvent;

import com.na.game.GameManager;
import com.na.game.event.impl.EventAdapter;
import com.na.game.event.impl.TouchEvent;
import com.na.game.sound.SoundManager;

public abstract class NormalEventListener extends EventAdapter {
	@Override
	public boolean onTouch(TouchEvent event) {
		if (event.action == MotionEvent.ACTION_DOWN) {
			GameManager.get(SoundManager.class).play(GameConfigEx.RES_SOUND_CLICK);
		}
		return false;
	}
}
