package com.na.flipcard.ui;

import android.graphics.Color;
import android.graphics.Rect;
import android.view.MotionEvent;

import com.na.flipcard.util.GameConfigEx;
import com.na.flipcard.util.GameUtil;
import com.na.game.GameActivity;
import com.na.game.GameMain;
import com.na.game.GameManager;
import com.na.game.event.EventListener;
import com.na.game.event.Updater;
import com.na.game.event.impl.EventAdapter;
import com.na.game.event.impl.KeyEvent;
import com.na.game.event.impl.TouchEvent;
import com.na.game.graphics.Image;
import com.na.game.platform.ad.Ad;
import com.na.game.platform.pay.PayManager;
import com.na.game.platform.sns.SnsManager;
import com.na.game.resource.ResourceManager;
import com.na.game.sound.SoundManager;
import com.na.game.ui.UI;
import com.na.game.ui.UIManager;
import com.na.game.util.GameConfig;
import com.na.game.util.Timer;
import com.na.game.util.TimerTask;
import com.na.game.util.Util;
import com.na.game.widget.Layout;
import com.na.game.widget.impl.Container;
import com.na.game.widget.impl.Icon;
import com.na.game.widget.impl.Label;

public class WelcomeUI extends UI {

	private boolean needCheckWhiteUser = false;
	
	@Override
	public void init() {
		GameManager.get(SoundManager.class).load(GameConfigEx.RES_SOUND_CLICK2, false);
		Container ui = new Container();
		ui.setBound(0, 0, GameConfig.STANDARD_WIDTH, GameConfig.STANDARD_HEIGHT);
		setWidget(ui);
		
		ResourceManager rm = GameManager.get(ResourceManager.class);
		
		Image[] images = new Image[]{
				rm.getImageFromAssets(GameConfigEx.RES_NAME_TIAN),
				rm.getImageFromAssets(GameConfigEx.RES_NAME_TIAN),
				rm.getImageFromAssets(GameConfigEx.RES_NAME_KA),
				rm.getImageFromAssets(GameConfigEx.RES_NAME_PIAN),
		};
		int w = images[0].getWidth();
		int h = images[0].getHeight();
		int x = (GameConfig.STANDARD_WIDTH - 4 * w) / 2;
		int y = GameConfig.STANDARD_HEIGHT / 2 - h - 100;
		Icon[] icons = new Icon[images.length];
		for (int i = 0; i < images.length; i++) {
			icons[i] = new Icon(images[i]);
			ui.addChild(icons[i]);
			icons[i].setBound(x, -h, w, h);
			x += w;
		}
		ui.setUpdater(new Animation(icons, new Rect(x, y, w, h)));
		
		x = (GameConfig.STANDARD_WIDTH - Util.getTextWidth("加载中...")) >> 1;
		final Label tip = new Label("加载中", Color.WHITE);
		ui.addChild(tip);
		tip.setTextAlign(Layout.LEFT_CENTER);
		tip.setBound(x, 0, GameConfig.STANDARD_WIDTH - x, GameConfig.STANDARD_HEIGHT);
		tip.setUpdater(new Updater() {
			int tick = 0;
			@Override
			public void update() {
				String text = null;
				switch (tick) {
					case 0:
						text = "加载中";
						break;
					case 8:
						text = "加载中.";
						break;
					case 16:
						text = "加载中..";
						break;
					case 24:
						text = "加载中...";
						break;
				}
				if (text != null)
					tip.setText(text);
				if (++tick >= 32) {
					tick = 0;
				}
			}
		});
		
		
		final GameActivity ctx = GameManager.get(GameActivity.class);
		String pid = Util.getAppMetaString(ctx, "WAPS_PID");
		needCheckWhiteUser = "hiapk".equals(pid) || "goapk".equals(pid); // 使用白名单：安卓、安智
		ctx.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				GameManager.add(new PayManager(ctx));
				GameManager.add(new SnsManager(ctx));
				Ad.getAd().init(ctx);
				new Timer().schedule(new TimerTask() {
					@Override
					public void execute() {
						GameUtil.cacheBmob(null); // try first time
						if (needCheckWhiteUser) {
							GameConfigEx.isWhiteUser = Boolean.valueOf(Ad.getAd().getConfig(GameConfigEx.WAPS_CONFIG_WHITEUSER, null));
							gotoMenu();
						}
					}
				}, 1000);
			}
		});
	}
	
	private void gotoMenu() {
		GameManager.get(UIManager.class).popUI();
		GameManager.get(UIManager.class).pushUI(new MenuUI());
	}
	
	@Override
	public boolean onKey(KeyEvent event) {
		if (event.action == android.view.KeyEvent.ACTION_UP && event.keyCode == android.view.KeyEvent.KEYCODE_BACK) {
			exitGame();
			return true;
		}
		return false;
	}
	
	private void exitGame() {
		String title = "提示";
		String msg = "确定退出游戏吗?";
		String ok = "确定";
		String cancel = "取消";
		EventListener okListener = new EventAdapter() {
			@Override
			public boolean onTouch(TouchEvent event) {
				if (event.action == MotionEvent.ACTION_UP) {
					GameManager.get(GameMain.class).stop();
				} else if (event.action == MotionEvent.ACTION_DOWN) {
					GameManager.get(SoundManager.class).play(GameConfigEx.RES_SOUND_CLICK);
				}
				return true;
			}
		};
		DialogUI dialog = new DialogUI(title, msg, ok, okListener, cancel, null) {
			@Override
			public void init() {
				super.init();
				uiWidget.setPainter(null); // set empty
			}
		};
		GameManager.get(UIManager.class).pushUI(dialog);
	}
	
	class Animation implements Updater {
		private Icon[] icons;
		private Rect target;
		private int rate = 10;
		private int index = 0;
		Animation(Icon[] icons, Rect target) {
			this.icons = icons;
			this.target = target;
		}
		
		@Override
		public void update() {
			if (!GameManager.get(SoundManager.class).isLoaded(GameConfigEx.RES_SOUND_CLICK2)) {
				return;
			}
			if (index >= icons.length) {
				if (!needCheckWhiteUser) {
					gotoMenu();
				}
				return;
			}
			Rect rect = icons[index].getBound();
			if (rect.top >= target.top) {
				index++;
				rate = 10;
				GameManager.get(SoundManager.class).play(GameConfigEx.RES_SOUND_CLICK2);
				return;
			}
			rect.top += rate;
			rect.bottom += rate;
			icons[index].setBound(rect);
			rate += 4;
		}
		
	}

}
