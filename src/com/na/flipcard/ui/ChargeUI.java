package com.na.flipcard.ui;

import java.util.ArrayList;
import java.util.List;

import android.graphics.Color;
import android.graphics.Rect;
import android.view.MotionEvent;

import com.na.flipcard.util.GameConfigEx;
import com.na.flipcard.util.GameUtil;
import com.na.flipcard.util.NormalEventListener;
import com.na.flipcard.util.UpdateAdPointListener;
import com.na.game.GameManager;
import com.na.game.event.impl.TouchEvent;
import com.na.game.graphics.Image;
import com.na.game.platform.ad.Ad;
import com.na.game.platform.ad.AdPoints;
import com.na.game.platform.pay.PayListener;
import com.na.game.platform.pay.PayManager;
import com.na.game.resource.ResourceManager;
import com.na.game.ui.UI;
import com.na.game.util.GameConfig;
import com.na.game.util.Util;
import com.na.game.util.Util.NormalBgPainter;
import com.na.game.widget.impl.Button;
import com.na.game.widget.impl.Container;
import com.na.game.widget.impl.Label;

public class ChargeUI extends UI {

	private Label myGold;
	
	@Override
	public void init() {
		Ad.getAd().getPoints(new PointsListener());
		
		int x = 0, y = 0, w = GameConfig.STANDARD_WIDTH, h = GameConfig.STANDARD_HEIGHT;
		Container ui = new Container();
		ui.setBound(x, y, w, h);
		ui.setPainter(new NormalBgPainter(ui));
		setWidget(ui);
		
		// title
		x = 0;
		y = 10;
		h = 100;
		Label title = new Label("请选择充值金额", true, GameConfigEx.COLOR_GREEN, Color.WHITE, GameConfigEx.TEXT_SIZE_XLARGE);
		ui.addChild(title);
		title.setBound(x, y, w, h);
		// desc
		y += h;
		h = Util.getTextHeight("充", 30);
		Label desc = new Label("充得越多,送得越多哦", GameConfigEx.COLOR_GREEN);
		ui.addChild(desc);
		desc.setTextSize(GameConfigEx.TEXT_SIZE_SMALL);
		desc.setBound(x, y, w, h);
		// my point
		y += h;
		myGold = new Label("", GameConfigEx.COLOR_GREEN);
		ui.addChild(myGold);
		myGold.setTextSize(GameConfigEx.TEXT_SIZE_SMALL);
		myGold.setBound(x, y, w, h);
		
		List<Charge> config = getConfig();
		int size = config.size();
		ResourceManager rm = GameManager.get(ResourceManager.class);
		Image[] images = new Image[]{
				rm.getImageFromAssets(GameConfigEx.RES_LONG_BUTTON),
				rm.getImageFromAssets(GameConfigEx.RES_LONG_BUTTON_PRESSED)
		};
		y += h;
		int offset = 10;
		w = ui.getBound().width();//images[0].getWidth();
		h = images[0].getHeight();
		x = (GameConfig.STANDARD_WIDTH - w) >> 1;
//		y += (GameConfig.STANDARD_HEIGHT - y - size * h - (size - 1) * offset) >> 1;
		for (int i = 0; i < size; i++) {
			Button button = new Button(images, config.get(i).desc, 0xff12a495);
			ui.addChild(button);
			button.setBound(x, y, w, h);
			button.setImageColor(0xffc8d7b3);
			button.setEventListener(new ButtonListener(config.get(i)));
			button.setTextSize(GameConfigEx.TEXT_SIZE_MEDIUM);
			button.setScaled(true);
			button.setSrcRect(new Rect(0, 0, images[0].getWidth(), images[0].getHeight()));
			y += h + offset;
		}
	}
	
	private List<Charge> getConfig() {
		List<Charge> config = new ArrayList<Charge>();
		config.add(new Charge(1f, 200, "1元=200个金币"));
		config.add(new Charge(5f, 200 * 5 + 50, "5元=1000个金币+50个金币"));
		config.add(new Charge(10f, 200 * 10 + 150, "10元=2000个金币+150个金币"));
		config.add(new Charge(50f, 200 * 50 + 1000, "50元=10000个金币+1000个金币"));
		return config;
	}
	
	class Charge {
		float money;
		int point;
		String desc;
		Charge(float money, int point, String desc) {
			this.money = money;
			this.point = point;
			this.desc = desc;
		}
		
		@Override
		public String toString() {
			return "[Charge]money: " + money + " , point: " + point + " , desc: " + desc;
		}
	}
	
	class ButtonListener extends NormalEventListener {
		private Charge charge;
		ButtonListener(Charge charge) {
			this.charge = charge;
		}
		
		@Override
		public boolean onTouch(TouchEvent event) {
			super.onTouch(event);
			if (event.action == MotionEvent.ACTION_UP) {
				String orderId = String.valueOf(System.currentTimeMillis());
				GameManager.get(PayManager.class).pay(orderId, charge.money, charge.desc, new PayResultListener(charge));
			}
			return true;
		}
	}
	
	class PayResultListener implements PayListener {
		private Charge charge;
		PayResultListener(Charge charge) {
			this.charge = charge;
		}
		@Override
		public void onPayFinish(String orderId, int resultCode, String resultString, float money) {
			if (resultCode == SUCCESS) {
				int point = money == charge.money ? charge.point : GameUtil.calcPointByMoney(money);
				Ad.getAd().awardPoints(point, new PointsListener());
				Util.showToast("充值成功,系统已为您发放" + point + "个金币");
			} else {
				Util.showToast("充值失败:" + resultString);
			}
		}
	}
	
	class PointsListener extends UpdateAdPointListener {
		@Override
		public void notify(int type, AdPoints param) {
			super.notify(type, param);
			if (type == TYPE_AD_POINTS && myGold != null) {
				myGold.setText("我的金币: " + param.point);
			}
		}
	}

}
