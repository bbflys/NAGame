package com.na.flipcard.ui;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Rect;
import android.view.MotionEvent;

import com.na.flipcard.Game;
import com.na.flipcard.util.GameConfigEx;
import com.na.flipcard.util.NormalEventListener;
import com.na.game.GameManager;
import com.na.game.event.impl.KeyEvent;
import com.na.game.event.impl.TouchEvent;
import com.na.game.graphics.Graphics;
import com.na.game.graphics.Image;
import com.na.game.graphics.Painter;
import com.na.game.props.PropsManager;
import com.na.game.resource.ResourceManager;
import com.na.game.ui.UI;
import com.na.game.ui.UIManager;
import com.na.game.util.GameConfig;
import com.na.game.widget.Layout;
import com.na.game.widget.impl.Button;
import com.na.game.widget.impl.Container;
import com.na.game.widget.impl.Label;

public class PauseUI extends UI {

	private Game game;
	public PauseUI(Game game) {
		this.game = game;
	}
	
	@Override
	public void init() {
		setTransparent(true);
		Container ui = new Container();
		ui.setBound(0, 0, GameConfig.STANDARD_WIDTH, GameConfig.STANDARD_HEIGHT);
		setWidget(ui);
		
		int w = 680;
		int h = 450;
		int x = (GameConfig.STANDARD_WIDTH - w) >> 1;
		int y = (GameConfig.STANDARD_HEIGHT - h) >> 1;
		final Container uiC = new Container();
		ui.addChild(uiC);
		uiC.setBound(x, y, w, h);
		uiC.setPainter(new Painter() {
			private Image image = GameManager.get(ResourceManager.class).getImageFromAssets(GameConfigEx.RES_GAME_PANEL2);
			@Override
			public void paint(Graphics g) {
				Rect dst = new Rect(uiC.getAbsBound());
				dst.top += 80;
				g.setColorFilter(0xffc8d7b3);
				g.drawImage(image, new Rect(0, 0, image.getWidth(), image.getHeight()), dst);
				g.setColorFilter(Graphics.COLOR_FILTER_NULL);
			}
		});
		
		ResourceManager rm = GameManager.get(ResourceManager.class);
		
		x = y = 0;
		h = 100;
		Image image = rm.getImageFromAssets(GameConfigEx.RES_GAME_PANEL);
		Button titleBtn = new Button(image, "游戏暂停", Color.BLACK);
		titleBtn.setTextSize(GameConfigEx.TEXT_SIZE_SMALL);
		uiC.addChild(titleBtn);
		titleBtn.setBound(0, 0, w, h);
		titleBtn.setScaled(true);
		titleBtn.setSrcRect(new Rect(0, 0, image.getWidth(), image.getHeight()));
		titleBtn.setImageColor(0xffa68039);
		
		y += h;
		h = 200;
		Label contentBtn = new Label("不要离开太久了哦", Color.BLACK);
		uiC.addChild(contentBtn);
		contentBtn.setTextAlign(Layout.CENTER);
		contentBtn.setTextSize(GameConfigEx.TEXT_SIZE_SMALL);
		contentBtn.setBound(x, y, w, h);
		contentBtn.setName("content btn");
		
		y += h;
		x = (w - 240 * 2) / 3;
		w = 240;
		h = 100;
		Image[] images = new Image[]{rm.getImageFromAssets(GameConfigEx.RES_BUTTON), rm.getImageFromAssets(GameConfigEx.RES_BUTTON_PRESSED)};
		Button exit = new Button(images, "退出游戏", Color.BLACK);
		uiC.addChild(exit);
		exit.setScaled(true);
		exit.setSrcRect(new Rect(0, 0, images[0].getWidth(), images[0].getHeight()));
		exit.setTextSize(GameConfigEx.TEXT_SIZE_SMALL);
		exit.setBound(x, y, w, h);
		exit.setImageColor(0xffa68039);
		exit.setEventListener(new ExitListener());
		
		x += w + x;
		Button resume = new Button(images, "继续游戏", Color.BLACK);
		uiC.addChild(resume);
		resume.setScaled(true);
		resume.setSrcRect(new Rect(0, 0, images[0].getWidth(), images[0].getHeight()));
		resume.setTextSize(GameConfigEx.TEXT_SIZE_SMALL);
		resume.setBound(x, y, w, h);
		resume.setImageColor(0xffa68039);
		resume.setEventListener(new ResumeListener());
	}
	
	@Override
	public boolean onKey(KeyEvent event) {
		if (event.action == android.view.KeyEvent.ACTION_UP && event.keyCode == android.view.KeyEvent.KEYCODE_BACK) {
			gameResume();
		}
		return true;
	}
	
	private void gameResume() {
		GameManager.get(UIManager.class).popUI();
		game.resume();
	}
	
	class ExitListener extends NormalEventListener {
		@Override
		public boolean onTouch(TouchEvent event) {
			super.onTouch(event);
			if (event.action == MotionEvent.ACTION_UP) {
				GameManager.get(PropsManager.class).clear();
				GameManager.get(UIManager.class).popUI(); // pop pause ui
				GameManager.get(UIManager.class).popUI(); // pop game ui
			}
			return true;
		}
	}
	
	class ResumeListener extends NormalEventListener {
		@Override
		public boolean onTouch(TouchEvent event) {
			super.onTouch(event);
			if (event.action == MotionEvent.ACTION_UP) {
				gameResume();
			}
			return true;
		}
	}

}
