package com.na.flipcard.ui;

import android.graphics.Matrix;
import android.graphics.Rect;
import android.view.MotionEvent;

import com.na.flipcard.util.GameConfigEx;
import com.na.flipcard.util.GameUtil;
import com.na.game.GameMain;
import com.na.game.GameManager;
import com.na.game.event.EventListener;
import com.na.game.event.Updater;
import com.na.game.event.impl.EventAdapter;
import com.na.game.event.impl.KeyEvent;
import com.na.game.event.impl.TouchEvent;
import com.na.game.graphics.Graphics;
import com.na.game.graphics.Image;
import com.na.game.resource.ResourceManager;
import com.na.game.sound.SoundManager;
import com.na.game.ui.UI;
import com.na.game.ui.UIManager;
import com.na.game.util.GameConfig;
import com.na.game.util.Util.NormalBgPainter;
import com.na.game.widget.impl.Button;
import com.na.game.widget.impl.Container;
import com.na.game.widget.impl.Icon;

public class MenuUI extends UI {

	@Override
	public void init() {
		GameUtil.cacheBmob(null);
		Container ui = new Container();
		ui.setBound(0, 0, GameConfig.STANDARD_WIDTH, GameConfig.STANDARD_HEIGHT);
		ui.setPainter(new NormalBgPainter(ui));
		setWidget(ui);
		
		ResourceManager rm = GameManager.get(ResourceManager.class);
		Image[] images = new Image[]{
				rm.getImageFromAssets(GameConfigEx.RES_NAME_TIAN),
				rm.getImageFromAssets(GameConfigEx.RES_NAME_TIAN),
				rm.getImageFromAssets(GameConfigEx.RES_NAME_KA),
				rm.getImageFromAssets(GameConfigEx.RES_NAME_PIAN),
		};
		int w = images[0].getWidth();
		int h = images[0].getHeight();
		int x = (GameConfig.STANDARD_WIDTH - 4 * w) / 2;
		int y = 300;
		TitleIcon[] icons = new TitleIcon[images.length];
		for (int i = 0; i < images.length; i++) {
			icons[i] = new TitleIcon(images[i]);
			ui.addChild(icons[i]);
			icons[i].setBound(x, y, w, h);
			x += w;
		}
		ui.setUpdater(new TitleAnimation(icons));
		
		images = new Image[]{
				rm.getImageFromAssets(GameConfigEx.RES_LONG_BUTTON),
				rm.getImageFromAssets(GameConfigEx.RES_LONG_BUTTON_PRESSED)
		};
		w = images[0].getWidth();
		h = images[0].getHeight();
		x = (GameConfig.STANDARD_WIDTH - w) >> 1;
		y = (GameConfig.STANDARD_HEIGHT - h * 2 - 20) >> 1;
		Button start = new Button(images, "开始", GameConfigEx.COLOR_MENU_FONT);
		ui.addChild(start);
		start.setEventListener(new MenuTouchListener(0));
		start.setBound(x, y, w, h);
		start.setImageColor(0xffc8d7b3);
		start.setName("start");
		
		y += h + 20;
		Button help = new Button(images, "帮助", GameConfigEx.COLOR_MENU_FONT);
		ui.addChild(help);
		help.setEventListener(new MenuTouchListener(1));
		help.setBound(x, y, w, h);
		help.setImageColor(0xffc8d7b3);
		help.setName("help");
		
//		Object data = GameManager.get(DataManager.class).get(GameConfigEx.DATA_FIRST_IN);
//		if (data == null) {
//			GameManager.get(DataManager.class).put(GameConfigEx.DATA_FIRST_IN, "1");
//			GameManager.get(UIManager.class).pushUI(new GuideUI());
//		}
	}
	
	@Override
	public boolean onKey(KeyEvent event) {
		if (event.action == android.view.KeyEvent.ACTION_UP && event.keyCode == android.view.KeyEvent.KEYCODE_BACK) {
			exitGame();
			return true;
		}
		return false;
	}
	
	private void exitGame() {
		String title = "提示";
		String msg = "确定退出游戏吗?";
		String ok = "确定";
		String cancel = "取消";
		EventListener okListener = new EventAdapter() {
			@Override
			public boolean onTouch(TouchEvent event) {
				if (event.action == MotionEvent.ACTION_UP) {
					GameManager.get(GameMain.class).stop();
				} else if (event.action == MotionEvent.ACTION_DOWN) {
					GameManager.get(SoundManager.class).play(GameConfigEx.RES_SOUND_CLICK);
				}
				return true;
			}
		};
		DialogUI dialog = new DialogUI(title, msg, ok, okListener, cancel, null);
		GameManager.get(UIManager.class).pushUI(dialog);
	}
	
	class MenuTouchListener extends EventAdapter {
		private int pos;
		public MenuTouchListener(int pos) {
			this.pos = pos;
		}
		@Override
		public boolean onTouch(TouchEvent event) {
			if (event.action == MotionEvent.ACTION_UP) {
				switch (pos) {
					case 0: // start game
						GameManager.get(UIManager.class).pushUI(new StartGameUI());
						break;
					case 1: // help
						GameManager.get(UIManager.class).pushUI(new HelpUI());
						break;
				}
			} else if (event.action == MotionEvent.ACTION_DOWN) {
				GameManager.get(SoundManager.class).play(GameConfigEx.RES_SOUND_CLICK);
			}
			return true;
		}
	}
	
	class TitleIcon extends Icon {
		protected Matrix matrix;
		public TitleIcon(Image image) {
			super(image);
		}
		
		@Override
		public void setBound(int x, int y, int width, int height) {
			super.setBound(x, y, width, height);
			matrix = new Matrix();
			matrix.setTranslate(absBound.left, absBound.top);
		}

		@Override
		public void paint(Graphics g) {
//			reset(g);
			g.drawImage(image, matrix);
		}
		
	}
	
	class TitleAnimation implements Updater {
		private TitleIcon[] icons;
		private int index;
		private int period = 1000;
		private int sleep = period;
		private int preUpdateTime = (int) System.currentTimeMillis();
		
		private int degrees; // -15~15
		private int step; // 0~5:-3 6~11:+3 12~17:+3 18~23:-3
		TitleAnimation(TitleIcon[] icons) {
			this.icons = icons;
		}
		@Override
		public void update() {
			int elapse = (int) (System.currentTimeMillis() - preUpdateTime);
			preUpdateTime = (int) System.currentTimeMillis();
			if (sleep > 0) {
				sleep -= elapse;
				return;
			}
			
			Rect rect = icons[index].getAbsBound();
			Matrix matrix = icons[index].matrix;
			matrix.setTranslate(rect.left, rect.top);
			matrix.postRotate(degrees, rect.left + rect.width() / 2, rect.top + rect.height() / 2);
			step++;
			if (step >= 24) {
				step = 0;
				degrees = 0;
				index++;
				// if sleep
				if (index >= icons.length) {
					index = 0;
					sleep = period;
				}
				return;
			}
			
			if ((step > 0 && step <= 5) || (step > 18 && step <= 23)) {
				degrees -= 3;
			} else if ((step > 6 && step <= 11) || (step > 12 && step <= 17)) {
				degrees += 3;
			}
		}
		
	}
	
}
