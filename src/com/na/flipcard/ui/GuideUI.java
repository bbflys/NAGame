package com.na.flipcard.ui;

import com.na.game.ui.UI;
import com.na.game.util.GameConfig;
import com.na.game.util.Util;
import com.na.game.widget.impl.Container;
import com.na.game.widget.impl.Label;
import com.na.game.widget.impl.TextArea;

@Deprecated
public class GuideUI extends UI {

	@Override
	public void init() {
		Container c = new Container();
		c.setBound(0, 0, GameConfig.STANDARD_WIDTH, GameConfig.STANDARD_HEIGHT);
		setWidget(c);
		
		int w = 680;
		int h = 600;
		int x = (GameConfig.STANDARD_WIDTH - w) / 2;
		int y = (GameConfig.STANDARD_HEIGHT - h) / 2;
		Container ui = new Container();
		c.addChild(ui);
		ui.setBound(x, y, w, h);
		
		w = 660;
		x = 10;
		y = 10;
		h = Util.getTextHeight("高") * 3;
		TextArea tip = new TextArea("貌似你是第一次玩哦，先来看看怎么玩吧");
		ui.addChild(tip);
		tip.setBound(x, y, w, h);
		
		y += h;
		w = 300;
		x = (ui.getBound().width() - 2 * w) / 3;
		h = 100;
		Label go = new Label("看看去");
		ui.addChild(go);
		go.setBound(x, y, w, h);
		
		x += w + x;
		Label no = new Label("我很NB");
		ui.addChild(no);
		no.setBound(x, y, w, h);
	}

}
