package com.na.flipcard.ui;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.view.MotionEvent;

import com.na.flipcard.TopsActivity;
import com.na.flipcard.util.GameConfigEx;
import com.na.flipcard.util.NormalEventListener;
import com.na.game.GameActivity;
import com.na.game.GameManager;
import com.na.game.event.impl.TouchEvent;
import com.na.game.graphics.Image;
import com.na.game.platform.ad.Ad;
import com.na.game.resource.ResourceManager;
import com.na.game.ui.UI;
import com.na.game.ui.UIManager;
import com.na.game.util.GameConfig;
import com.na.game.util.Util.NormalBgPainter;
import com.na.game.widget.impl.Button;
import com.na.game.widget.impl.Container;

public class HelpUI extends UI {

	@Override
	public void init() {
		int x = 0, y = 0, w = GameConfig.STANDARD_WIDTH, h = GameConfig.STANDARD_HEIGHT;
		Container ui = new Container();
		ui.setBound(x, y, w, h);
		ui.setPainter(new NormalBgPainter(ui));
		setWidget(ui);
		
		ResourceManager rm = GameManager.get(ResourceManager.class);
		Image[] images = new Image[]{
				rm.getImageFromAssets(GameConfigEx.RES_LONG_BUTTON),
				rm.getImageFromAssets(GameConfigEx.RES_LONG_BUTTON_PRESSED)
		};
		List<String> names = new ArrayList<String>(6);
		names.add("排行榜");
		names.add("怎么玩");
		names.add("反馈");
		if (!GameConfigEx.isWhiteUser) {
			names.add("赚取金币");
			names.add("团购赚金币");
		}
		names.add("充值");
		String[] items = names.toArray(new String[names.size()]);
		int offset = 10;
		w = images[0].getWidth();
		h = images[0].getHeight();
		x = (GameConfig.STANDARD_WIDTH - w) >> 1;
		y = (GameConfig.STANDARD_HEIGHT - items.length * h - (items.length - 1) * offset) >> 1;
		for (int i = 0; i < items.length; i++) {
			Button button = new Button(images, items[i], 0xff12a495);
			ui.addChild(button);
			button.setBound(x, y, w, h);
			button.setImageColor(0xffc8d7b3);
			button.setEventListener(new ButtonListener(items[i]));
			y += h + offset;
		}
	}

	class ButtonListener extends NormalEventListener {
		private String text;
		ButtonListener(String text) {
			this.text = text;
		}
		@Override
		public boolean onTouch(TouchEvent event) {
			super.onTouch(event);
			if (event.action == MotionEvent.ACTION_UP) {
				if ("排行榜".equals(text)) {
					GameActivity context = GameManager.get(GameActivity.class);
					Intent intent = new Intent(context, TopsActivity.class);
					context.startActivity(intent);
				} else if ("怎么玩".equals(text)) {
					GameManager.get(UIManager.class).pushUI(new TeachPlayUI());
				} else if ("反馈".equals(text)) {
					Ad.getAd().showFeedback();
				} else if ("赚取金币".equals(text)) {
					Ad.getAd().showOffers();
				} else if ("团购赚金币".equals(text)) {
					Ad.getAd().showTuanOffers();
				} else {
					GameManager.get(UIManager.class).pushUI(new ChargeUI());
				}
			}
			return true;
		}
	}
}
