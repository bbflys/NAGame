package com.na.flipcard.ui;

import android.graphics.Rect;

import com.na.flipcard.util.GameConfigEx;
import com.na.game.GameManager;
import com.na.game.graphics.Graphics;
import com.na.game.graphics.Image;
import com.na.game.graphics.Painter;
import com.na.game.resource.ResourceManager;
import com.na.game.ui.UI;
import com.na.game.util.GameConfig;
import com.na.game.util.Util.NormalBgPainter;
import com.na.game.widget.Layout;
import com.na.game.widget.impl.Container;
import com.na.game.widget.impl.TextArea;

public class TeachPlayUI extends UI {

	@Override
	public void init() {
		int x = 0, y = 0, w = GameConfig.STANDARD_WIDTH, h = GameConfig.STANDARD_HEIGHT;
		Container ui = new Container();
		ui.setBound(x, y, w, h);
		ui.setPainter(new NormalBgPainter(ui));
		setWidget(ui);
		
		x = 50;
		w -= 2 * x;
		String text = "1.翻开两张相同的图片则得分，每升一级得分率相应升级" +
				"\n\n2.每局游戏限时60秒，每升一级奖励时间5秒" +
				"\n\n3.每局游戏最多可使用4种不同的道具，每种道具最多使用1~3个，游戏结束后，若有道具未使用，系统会自动清空所有道具" +
				"\n\n4.选择道具界面中，按住道具后左右滑动可选择道具数量" +
				"\n\n5.使用手机音量键可调节游戏中的音效音量" +
				"\n\n6.声明:游戏中的所有音效、图片素材均来自网络...呵呵";
		final TextArea tip = new TextArea(text, GameConfigEx.COLOR_MENU_FONT);
		ui.addChild(tip);
		tip.setTextAlign(Layout.LEFT_CENTER);
		tip.setBound(x, y, w, h);
		tip.setTextSize(GameConfigEx.TEXT_SIZE_MEDIUM);

		final Image image = GameManager.get(ResourceManager.class).getImageFromAssets(GameConfigEx.RES_GAME_PANEL);
		tip.setPainter(new Painter() {
			@Override
			public void paint(Graphics g) {
				Rect rect = new Rect(tip.getAbsBound());
				rect.left -= 40;
				rect.right += 40;
				g.setClip(rect.left, rect.top, rect.width(), rect.height());
				g.setColorFilter(0xffc8d7b3);
				g.drawImage(image, new Rect(0, 0, image.getWidth(), image.getHeight()), rect);
				g.setColorFilter(Graphics.COLOR_FILTER_NULL);
			}
		});
	}

}
