package com.na.flipcard.ui;

import android.graphics.Color;
import android.graphics.Rect;
import android.view.MotionEvent;

import com.na.flipcard.util.GameConfigEx;
import com.na.game.GameManager;
import com.na.game.event.EventListener;
import com.na.game.event.impl.EventAdapter;
import com.na.game.event.impl.TouchEvent;
import com.na.game.graphics.Graphics;
import com.na.game.graphics.Image;
import com.na.game.graphics.Painter;
import com.na.game.resource.ResourceManager;
import com.na.game.sound.SoundManager;
import com.na.game.ui.UI;
import com.na.game.ui.UIManager;
import com.na.game.util.GameConfig;
import com.na.game.util.Util.NormalBgPainter;
import com.na.game.widget.Layout;
import com.na.game.widget.impl.Button;
import com.na.game.widget.impl.Container;
import com.na.game.widget.impl.Label;

public class DialogUI extends UI {

	private String title, msg, ok, cancel;
	private EventListener okListener, cancelListener;
	
	public DialogUI(String title, String msg, String ok, EventListener okListener, String cancel, EventListener cancelListener) {
		this.title = title;
		this.msg = msg;
		this.ok = ok;
		this.okListener = okListener;
		this.cancel = cancel;
		this.cancelListener = cancelListener;
	}
	
	@Override
	public void init() {
		Container ui = new Container();
		ui.setBound(0, 0, GameConfig.STANDARD_WIDTH, GameConfig.STANDARD_HEIGHT);
		ui.setPainter(new NormalBgPainter(ui));
		setWidget(ui);
		
		int w = 680;
		int h = 450;
		int x = (GameConfig.STANDARD_WIDTH - w) >> 1;
		int y = (GameConfig.STANDARD_HEIGHT - h) >> 1;
		final Container uiC = new Container();
		ui.addChild(uiC);
		uiC.setBound(x, y, w, h);
		uiC.setPainter(new Painter() {
			private Image image = GameManager.get(ResourceManager.class).getImageFromAssets(GameConfigEx.RES_GAME_PANEL2);
			@Override
			public void paint(Graphics g) {
				g.setColorFilter(0xffc8d7b3);
				g.drawImage(image, new Rect(0, 0, image.getWidth(), image.getHeight()), uiC.getAbsBound());
				g.setColorFilter(Graphics.COLOR_FILTER_NULL);
			}
		});
		
		ResourceManager rm = GameManager.get(ResourceManager.class);
		
		x = y = 0;
		h = 100;
		Label titleBtn = new Label(title, Color.BLACK);
		titleBtn.setTextSize(GameConfigEx.TEXT_SIZE_SMALL);
		uiC.addChild(titleBtn);
		titleBtn.setBound(0, 0, w, h);
		
		y += h;
		h = 200;
		Label contentBtn = new Label(msg, Color.BLACK);
		uiC.addChild(contentBtn);
		contentBtn.setTextAlign(Layout.CENTER);
		contentBtn.setTextSize(GameConfigEx.TEXT_SIZE_SMALL);
		contentBtn.setBound(x, y, w, h);
		contentBtn.setName("content btn");
		
		y += h;
		if (ok != null && cancel != null) {
			x = (w - 240 * 2) / 3;
		} else {
			x = (w - 240) / 2;
		}
		w = 240;
		h = 100;
		Image[] images = new Image[]{rm.getImageFromAssets(GameConfigEx.RES_BUTTON), rm.getImageFromAssets(GameConfigEx.RES_BUTTON_PRESSED)};
		if (ok != null) {
			Button okBtn = new Button(images, ok, Color.BLACK);
			uiC.addChild(okBtn);
			okBtn.setScaled(true);
			okBtn.setSrcRect(new Rect(0, 0, images[0].getWidth(), images[0].getHeight()));
			okBtn.setTextSize(GameConfigEx.TEXT_SIZE_SMALL);
			okBtn.setBound(x, y, w, h);
			okBtn.setEventListener(okListener == null ? new NullListener() : okListener);
			okBtn.setImageColor(0xffa68039);
			x += w + x;
		}
		if (cancel != null) {
			Button cancelBtn = new Button(images, cancel, Color.BLACK);
			uiC.addChild(cancelBtn);
			cancelBtn.setScaled(true);
			cancelBtn.setSrcRect(new Rect(0, 0, images[0].getWidth(), images[0].getHeight()));
			cancelBtn.setTextSize(GameConfigEx.TEXT_SIZE_SMALL);
			cancelBtn.setBound(x, y, w, h);
			cancelBtn.setEventListener(cancelListener == null ? new NullListener() : cancelListener);
			cancelBtn.setImageColor(0xffa68039);
		}
	}
	
	class NullListener extends EventAdapter {
		@Override
		public boolean onTouch(TouchEvent event) {
			switch (event.action) {
				case MotionEvent.ACTION_DOWN:
					GameManager.get(SoundManager.class).play(GameConfigEx.RES_SOUND_CLICK);
					break;
				case MotionEvent.ACTION_UP:
					GameManager.get(UIManager.class).popUI();
					break;
			}
			return true;
		}
	}

}
