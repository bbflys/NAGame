package com.na.flipcard.props;

import java.util.Timer;
import java.util.TimerTask;

import com.na.flipcard.Effect;
import com.na.flipcard.Game;
import com.na.game.graphics.Image;
import com.na.game.props.GameProps;

public class Finder extends GameProps implements Effect {

	public Finder(String name, String desc, int amount, Image icon, int price) {
		super(name, desc, amount, icon, price);
	}

	@Override
	protected boolean effect() {
		if (((Game) extra).getEffect() != null) {
			return false;
		}
		((Game) extra).setEffect(this);
		new Timer().schedule(new TimerTask() {
			@Override
			public void run() {
				((Game) extra).setEffect(null);
			}
		}, 2000);
		return true;
	}

	@Override
	public boolean isSelected(int gridIndex) {
		return true;
	}

	@Override
	public boolean isDither() {
		return true;
	}

	@Override
	public String getScoreTxt() {
		return null;
	}

	@Override
	public int getMul() {
		return 1;
	}

}
