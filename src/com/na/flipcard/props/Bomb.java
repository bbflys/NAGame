package com.na.flipcard.props;

import java.util.Random;

import android.util.SparseIntArray;

import com.na.flipcard.Game;
import com.na.game.graphics.Image;
import com.na.game.props.GameProps;

public class Bomb extends GameProps {

	public Bomb(String name, String desc, int amount, Image icon, int price) {
		super(name, desc, amount, icon, price);
	}

	@Override
	protected boolean effect() {
		Game game = (Game) extra;
		int[] idxs = rdx(game.getData());
		if (idxs != null) {
			game.bingo(idxs[0], idxs[1]);
			return true;
		}
		return false;
	}
	
	private int[] rdx(byte[] data) {
		SparseIntArray temp = new SparseIntArray(data.length);
		for (int i = data.length - 1; i >= 0; i--) {
			if (data[i] >= 0) {
				temp.put(i, data[i]);
			}
		}
		if (temp.size() == 0) {
			return null;
		}
		int idx = new Random().nextInt(temp.size());
		int k = temp.keyAt(idx);
		int v = temp.valueAt(idx);
		for (int i = temp.size() - 1; i >= 0; i--) {
			if (idx == i) { // exclude duplicate
				continue;
			}
			int k2 = temp.keyAt(i);
			int v2 = temp.valueAt(i);
			if (v2 == v) {
				return new int[]{k, k2};
			}
		}
		return null;
	}

}
