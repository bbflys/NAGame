package com.na.flipcard.props;

import com.na.flipcard.Game;
import com.na.game.graphics.Image;
import com.na.game.props.GameProps;

public class TimeBack extends GameProps {

	public TimeBack(String name, String desc, int amount, Image icon, int price) {
		super(name, desc, amount, icon, price);
	}

	@Override
	protected boolean effect() {
		return ((Game) extra).addTime(3000);
	}

}
