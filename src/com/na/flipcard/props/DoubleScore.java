package com.na.flipcard.props;

import com.na.flipcard.Effect;
import com.na.flipcard.Game;
import com.na.game.graphics.Image;
import com.na.game.props.GameProps;
import com.na.game.util.Util;

public class DoubleScore extends GameProps implements Effect {

	public DoubleScore(String name, String desc, int amount, Image icon, int price) {
		super(name, desc, amount, icon, price);
	}

	@Override
	protected boolean effect() {
		Util.showToast("双倍效果持续作用中...");
		return false;
	}

	@Override
	public boolean isSelected(int gridIndex) {
		return false;
	}

	@Override
	public boolean isDither() {
		return false;
	}

	@Override
	public String getScoreTxt() {
		return ((Game) extra).getScore() / getMul() + " x2";
	}

	@Override
	public int getMul() {
		return 2;
	}

}
