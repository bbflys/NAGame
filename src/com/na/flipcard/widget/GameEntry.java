package com.na.flipcard.widget;

import android.graphics.Matrix;
import android.graphics.Rect;
import android.view.MotionEvent;

import com.na.flipcard.Game;
import com.na.flipcard.util.GameConfigEx;
import com.na.game.GameManager;
import com.na.game.event.impl.TouchEvent;
import com.na.game.graphics.Graphics;
import com.na.game.graphics.Image;
import com.na.game.sound.SoundManager;
import com.na.game.util.PList;
import com.na.game.widget.impl.Grid;

public class GameEntry extends Grid {

	private Game game;
	private Image focus;

	public GameEntry(int index, Image focus, Game game) {
		this.index = index;
		this.focus = focus;
		syncUI(game);
	}
	
	public void syncUI(Game game) {
		byte[] data = game.getData();
		if (data[index] >= 0) {
			PList pl = game.getPList();
			this.image = pl.getImage();
			this.game = game;
			this.src = pl.getItem(data[index]).bound;
			this.scaled = true;
		} else {
			this.image = null;
			this.game = null;
			this.src = null;
			this.scaled = false;
		}
	}

	@Override
	public void paint(Graphics g) {
		if (game == null) {
			return;
		}
		if (image != null && (game.isSelected(index) || game.isBingo(index))) {
			super.paint(g);
		} else if (game.isDither()) {
			int offset = 12;
			Rect rect = new Rect(absBound);
			rect.left -= offset;
			rect.top -= offset;
			rect.right += offset;
			rect.bottom += offset;
			g.setClip(rect.left, rect.top, rect.width(), rect.height());
			
			Matrix matrix = new Matrix();
			matrix.setTranslate(rect.left, rect.top);
			matrix.postScale((float) rect.width() / focus.getWidth(), (float) rect.height() / focus.getHeight(), rect.left, rect.top);
			matrix.postRotate(game.getDitherDegrees(), rect.left + rect.width() / 2, rect.top + rect.height() / 2);
			g.drawImage(focus, matrix);
//			if (index == 1) {
//				matrix = new Matrix();
//				matrix.postRotate(degree, absBound.left + absBound.width() / 2, absBound.top + absBound.height() / 2);
//				matrix.setRectToRect(new RectF(src), new RectF(absBound), ScaleToFit.CENTER);
//				g.setClip(absBound.left, absBound.top, absBound.width(), absBound.height());
//				g.drawImage(image, matrix);
//			}
		} else {
			int offset = 12;
			Rect rect = new Rect(absBound);
			rect.left -= offset;
			rect.top -= offset;
			rect.right += offset;
			rect.bottom += offset;
			g.setClip(rect.left, rect.top, rect.width(), rect.height());
			g.drawImage(focus, new Rect(0, 0, focus.getWidth(), focus.getHeight()), rect);
		}
	}

	@Override
	public boolean onTouch(TouchEvent event) {
		super.onTouch(event);
		switch (event.action) {
			case MotionEvent.ACTION_DOWN:
				if (game != null) {
					GameManager.get(SoundManager.class).play(GameConfigEx.RES_SOUND_CLICK2);
				} else {
					GameManager.get(SoundManager.class).play(GameConfigEx.RES_SOUND_CLICK_BAN);
				}
				break;
			case MotionEvent.ACTION_UP:
				if (game != null) {
					game.onTouch(index);
				}
				break;
		}
		return true;
	}
}
