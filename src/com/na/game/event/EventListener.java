package com.na.game.event;

import com.na.game.event.impl.KeyEvent;
import com.na.game.event.impl.TouchEvent;

public interface EventListener {
	public boolean onKey(KeyEvent event);
	public boolean onTouch(TouchEvent event);
}
