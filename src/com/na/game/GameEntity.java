package com.na.game;


/**
 * 写这个抽象类的目的是替代游戏入口Activity，所以游戏内就不用自己再定义一个主Activity了，只要实现这个抽象类就行了<br>
 * 入口Activity替换为com.na.game.GameActivity
 * @author yang.li
 *
 */
public abstract class GameEntity {

	public static final int FUNC_GET_INIT_UI = 0;
	public static final int FUNC_GET_DEFAULT_FONT = 1;
	public static final int FUNC_ON_ACTIVITY_RESULT = 2;
	
	protected GameActivity context;
	public GameEntity(GameActivity context) {
		this.context = context;
	}
	
	public abstract <T> T invoke(int function, Class<T> returnType, Object... params);
}