package com.na.game.platform.sns;

import com.na.game.GameManager.Clazz;

import android.app.Activity;
import cn.bmob.Bmob;
import cn.bmob.BmobException;
import cn.bmob.BmobObject;
import cn.bmob.BmobQuery;
import cn.bmob.SaveCallback;

//import com.tencent.tauth.Constants;
//import com.tencent.tauth.IUiListener;
//import com.tencent.tauth.Tencent;
//import com.tencent.tauth.UiError;

public class SnsManager implements Clazz {

	private Activity ctx;
//	private Tencent tencent;
	private static final String BMOB_APP_ID = "097935ffd19ae3be3455183794e4e4bd";
	private static final String QQ_APP_ID = "100561035";
	private static final String QQ_SCOPE = "get_user_info,add_t,add_pic_t,add_share,add_topic";
	private QQUserInfo qqUserInfo; // cache
	
	public SnsManager(Activity ctx, Object... params/*String qqOpenId, String qqToken, String qqExpires*/) {
		this.ctx = ctx;
		Bmob.initialize(ctx, BMOB_APP_ID);
//		tencent = Tencent.createInstance(QQ_APP_ID, ctx);
//		if (qqOpenId != null) {
//			tencent.setOpenId(qqOpenId);
//		}
//		if (qqToken != null && qqExpires != null) {
//			tencent.setAccessToken(qqToken, qqExpires);
//		}
	}
	
	/**
	 * 
	 * @param obj
	 * @param table
	 * @param values key, value...
	 * @param listener
	 */
	public void save(BmobObject obj, String table, Object[] values, final SnsListener listener) {
		if (obj == null) {
			obj = new BmobObject(table);
		}
		for (int i = 0; i < values.length; i += 2) {
			obj.put(values[i].toString(), values[i + 1]);
		}
		obj.saveInBackground(new SaveCallback() {
			@Override
			public void done(BmobException e) {
				if (listener != null)
					listener.saveCallback(e != null ? e.getMessage() : null);
			}
		});
	}
	
	public void query(String table, SnsListener listener, Object... conditions) {
		BmobQuery query = new BmobQuery(table);
		if (conditions != null) {
			for (int i = 0; i < conditions.length; i += 2) {
				if (conditions[i] != null && conditions[i + 1] != null) {
					query.whereEqualTo(conditions[i].toString(), conditions[i + 1]);
				}
			}
		}
		query.findInBackground(new BmobFindCallback(listener));
	}
	
	public void query(String table, int limit, String orderCol, boolean asc, Integer factor, SnsListener listener) {
		BmobQuery query = new BmobQuery(table);
		query.setLimit(limit);
		if (asc) {
			if (factor != null) {
				query.whereGreaterThan(orderCol, factor);
			}
			query.orderByAscending(orderCol);
		} else {
			if (factor != null) {
				query.whereLessThan(orderCol, factor);
			}
			query.orderByDescending(orderCol);
		}
		query.findInBackground(new BmobFindCallback(listener));
	}
	
	public void queryGreatherThan(String table, String cols, int value, SnsListener listener) {
		BmobQuery query = new BmobQuery(table);
		query.whereGreaterThan(cols, value);
		query.setLimit(Integer.MAX_VALUE);
		query.findInBackground(new BmobFindCallback(listener));
	}
	
	@Deprecated
	public void login(final SnsListener listener) {
//		tencent.login(ctx, QQ_SCOPE, new IUiListener() {
//			@Override
//			public void onError(UiError error) {
//				listener.qqLogin(new QQLogin(error.errorCode, error.errorDetail, error.errorMessage));
//			}
//			
//			@Override
//			public void onComplete(JSONObject result) {
//				try {
//					String openId = result.getString("openid");
//					String accessToken = result.getString("access_token");
//					String expiresIn = result.getString("expires_in");
//					listener.qqLogin(new QQLogin(openId, accessToken, expiresIn));
//				} catch (Exception e) {}
//			}
//			
//			@Override
//			public void onCancel() {
//			}
//		});
	}
	
	@Deprecated
	public void logout() {
//		tencent.logout(ctx);
//		qqUserInfo = null;
	}
	
	@Deprecated
	public void getUserInfo(final SnsListener listener) {
//		if (qqUserInfo != null) {
//			listener.qqUserInfo(qqUserInfo);
//			return;
//		}
//		tencent.requestAsync(Constants.GRAPH_USER_INFO, null, Constants.HTTP_GET, new QQRequestListener() {
//			
//			@Override
//			public void success(JSONObject result, Object obj) {
//				try {
//					String nickname = result.getString("nickname");
//					String headIconUrl = result.getString("figureurl_qq_1");
//					qqUserInfo = new QQUserInfo(nickname, headIconUrl);
//					listener.qqUserInfo(qqUserInfo);
//				} catch (Exception e) {}
//			}
//			
//			@Override
//			public void error(String error) {
//			}
//		}, null);
	}
	
	@Deprecated
	public void shareToTencentWeibo(String content) {
//		Bundle bundle = new Bundle();
//		bundle.putString("format", "json");
//		bundle.putString("content", content);
//		
//		tencent.requestAsync("t/add_t"/*Constants.GRAPH_ADD_PIC_T*/, bundle, Constants.HTTP_POST, new QQRequestListener() {
//			
//			@Override
//			public void success(JSONObject result, Object obj) {
//			}
//			
//			@Override
//			public void error(String error) {
//			}
//		}, null);
	}
	
	@Deprecated
	public QQConnect getQQConnect() {
		return null;
	}
	
	@Override
	public Class<?> getClazz() {
		return getClass();
	}
	
}
