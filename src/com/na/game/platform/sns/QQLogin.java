package com.na.game.platform.sns;

@Deprecated
public class QQLogin {
	public static final int SUCCESS = 0;
	public static final int ERROR = 1;
	
	public int status; // SUCCESS or ERROR
	
	// success
	public String openId;
	public String accessToken;
	public String expiresIn;
	
	public QQLogin(String openId, String accessToken, String expiresIn) {
		status = SUCCESS;
		this.openId = openId;
		this.accessToken = accessToken;
		this.expiresIn = expiresIn;
	}
	
	// error
	public int errorCode;
	public String errorDetail;
	public String errorMessage;
	
	public QQLogin(int errorCode, String errorDetail, String errorMessage) {
		status = ERROR;
		this.errorCode = errorCode;
		this.errorDetail = errorDetail;
		this.errorMessage = errorMessage;
	}
	
	@Override
	public String toString() {
		if (status == SUCCESS) {
			return "[QQLogin]success[openId: " + openId + " , accessToken: " + accessToken + " , expiresIn: " + expiresIn + "]";
		}
		return "[QQLogin]error[errorCode: " + errorCode + " , errorDetail: " + errorDetail + " , errorMessage: " + errorMessage + "]";
	}
}
