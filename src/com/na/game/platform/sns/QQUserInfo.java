package com.na.game.platform.sns;

import com.na.game.util.Util;

@Deprecated
public class QQUserInfo {
	public String nickname;
	public String headIconUrl;
	public String deviceId;
	
	public QQUserInfo(String nickname, String headIconUrl) {
		this.nickname = nickname;
		this.headIconUrl = headIconUrl;
		deviceId = Util.getDeviceId();
	}
}
