package com.na.game.platform.sns;

import java.util.List;

import com.na.game.util.GameException;

import cn.bmob.BmobObject;

public abstract class SnsListener {
	public void saveCallback(String msg) {}
	public void queryResult(List<BmobObject> result, GameException ex) {}
	@Deprecated
	public void qqLogin(QQLogin result) {}
	@Deprecated
	public void qqUserInfo(QQUserInfo userInfo) {}
}
