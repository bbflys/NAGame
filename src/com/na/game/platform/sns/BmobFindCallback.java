package com.na.game.platform.sns;

import java.util.List;

import com.na.game.util.GameException;

import cn.bmob.BmobException;
import cn.bmob.BmobObject;
import cn.bmob.FindCallback;

public class BmobFindCallback extends FindCallback {

	private SnsListener listener;
	
	public BmobFindCallback(SnsListener listener) {
		this.listener = listener;
	}
	
	@Override
	public void done(List<BmobObject> result, BmobException ex) {
		if (listener != null)
			listener.queryResult(result, ex != null ? new GameException(ex) : null);
	}

}
