package com.na.game.platform.pay;

public interface PayListener {
	int SUCCESS = 0;
	public void onPayFinish(String orderId, int resultCode, String resultString, float money);
}
