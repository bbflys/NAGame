package com.na.game.platform.ad;

public interface AdListener<T> {
	
	int TYPE_AD_POINTS = 0;
	
	public void notify(int type, T param);
}
