package com.na.game.platform.ad;

import android.content.Context;
import android.view.View;
import cn.waps.AppConnect;
import cn.waps.UpdatePointsNotifier;


public class Waps extends Ad {

	private Context ctx;
	
	class PointsListener implements UpdatePointsNotifier {
		private AdListener<AdPoints> listener;
		private int pointType;
		PointsListener(AdListener<AdPoints> listener, int pointType) {
			this.listener = listener;
			this.pointType = pointType;
		}
		
		@Override
		public void getUpdatePoints(String currencyName, int amount) {
			if (listener != null) {
				listener.notify(AdListener.TYPE_AD_POINTS, new AdPoints(pointType, currencyName, amount));
			}
		}

		@Override
		public void getUpdatePointsFailed(String error) {
			if (listener != null) {
				listener.notify(AdListener.TYPE_AD_POINTS, new AdPoints(pointType, error));
			}
		}
		
	}
	
	@Override
	public void init(Context ctx) {
		this.ctx = ctx;
		AppConnect.getInstance(ctx);
		AppConnect.getInstance(ctx).initPopAd(ctx);
//		AppConnect.getInstance(ctx).checkUpdate(ctx);
	}

	@Override
	public void destroy() {
		AppConnect.getInstance(ctx).close();
	}

	@Override
	public void getPoints(AdListener<AdPoints> listener) {
		AppConnect.getInstance(ctx).getPoints(new PointsListener(listener, AdPoints.TYPE_GET_POINTS));
	}

	@Override
	public void spendPoints(int amount, AdListener<AdPoints> listener) {
		if (amount <= 0) {
			return;
		}
		AppConnect.getInstance(ctx).spendPoints(amount, new PointsListener(listener, AdPoints.TYPE_SPEND_POINTS));
	}

	@Override
	public void awardPoints(int amount, AdListener<AdPoints> listener) {
		if (amount <= 0) {
			return;
		}
		AppConnect.getInstance(ctx).awardPoints(amount, new PointsListener(listener, AdPoints.TYPE_AWARD_POINTS));
	}

	@Override
	public void showOffers() {
		AppConnect.getInstance(ctx).showOffers(ctx);
	}

	@Override
	public void showTuanOffers() {
		AppConnect.getInstance(ctx).showTuanOffers(ctx);
	}

	@Override
	public void showPopAd() {
		AppConnect.getInstance(ctx).showPopAd(ctx);
	}

	@Override
	public View getPopAdView(int width, int height) {
		return AppConnect.getInstance(ctx).getPopAdView(ctx, width, height);
	}

	@Override
	public void showFeedback() {
		AppConnect.getInstance(ctx).showFeedback();
	}

	@Override
	public void showMore() {
		AppConnect.getInstance(ctx).showMore(ctx);
	}

	@Override
	public String getConfig(String key, String defaultValue) {
		if (defaultValue == null) {
			return AppConnect.getInstance(ctx).getConfig(key);
		}
		return AppConnect.getInstance(ctx).getConfig(key, defaultValue);
	}

}
