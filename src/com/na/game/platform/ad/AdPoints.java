package com.na.game.platform.ad;

public class AdPoints {

	public static final int TYPE_GET_POINTS = 0;
	public static final int TYPE_SPEND_POINTS = 1;
	public static final int TYPE_AWARD_POINTS = 2;
	
	public int type;
	public String msg; // success->currencyName fail->msg
	public int point;
	
	public AdPoints(int type, String currencyName, int point) {
		this.type = type;
		this.msg = currencyName;
		this.point = point;
	}
	
	public AdPoints(int type, String msg) {
		this(type, msg, 0);
	}
}
