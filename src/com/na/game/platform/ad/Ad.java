package com.na.game.platform.ad;


import android.content.Context;
import android.view.View;

/**
 * 这样写是为了方便接入不同的广告平台
 * @author yang.li
 *
 */
public abstract class Ad {

	private static Ad ad;
	
	public static Ad getAd() {
		if (ad == null) {
			ad = new Waps();
		}
		return ad;
	}
	
	public abstract void init(Context ctx);
	public abstract void destroy();
	public abstract void getPoints(AdListener<AdPoints> listener);
	public abstract void spendPoints(int amount, AdListener<AdPoints> listener);
	public abstract void awardPoints(int amount, AdListener<AdPoints> listener);
	public abstract void showOffers();
	public abstract void showTuanOffers();
	public abstract void showPopAd();
	public abstract View getPopAdView(int width, int height);
	public abstract void showFeedback();
	public abstract void showMore();
	public abstract String getConfig(String key, String defaultValue);
}
