package com.na.game.graphics;

public interface Painter {
	public void paint(Graphics g);
}
