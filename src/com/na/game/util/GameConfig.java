package com.na.game.util;

import java.io.File;

import android.graphics.Bitmap;

public class GameConfig {
	public static int MSPF = 25; // millisecond per frame
	public static int STANDARD_WIDTH = 720;
	public static int STANDARD_HEIGHT = 1280;
	public static int FONT_DEFAULT_SIZE = 50;
	
	public static final String RES_PROPS_XML = "props/props.xml";
	public static final String RES_GAME_DATA = "com.nagame.supercard/na_game.dta";
	
	public static int DENSITY = Bitmap.DENSITY_NONE;
	public static File SDCARD = null;
	public static int screenWidth, screenHeight;
	public static boolean useOpenGL;
	public static final boolean DEBUG = true;
}
