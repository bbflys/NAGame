package com.na.game.widget.impl;

import com.na.game.graphics.Graphics;
import com.na.game.graphics.Image;
import com.na.game.widget.Layout;
import com.na.game.widget.Widget;

public class Grid extends Widget {

	protected int index; // in grid panel
	protected Image image;
	
	protected Grid() {}
	
	public Grid(Image image, int index) {
		this.image = image;
		this.index = index;
	}

	@Override
	public void paint(Graphics g) {
		if (!visible) {
			return;
		}
		super.paint(g);
		if (scaled) {
			g.drawImage(image, src, absBound);
		} else {
			g.drawImage(image, absBound.left, absBound.top, Layout.TOP_LEFT);
		}
	}
}
