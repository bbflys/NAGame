package com.na.game.widget.impl;

import com.na.game.graphics.Graphics;
import com.na.game.graphics.Image;
import com.na.game.widget.Layout;
import com.na.game.widget.Widget;

public class Icon extends Widget {

	protected Image image;
	
	public Icon(Image image) {
		this.image = image;
	}
	
	public void setIcon(Image image) {
		this.image = image;
	}

	@Override
	public void paint(Graphics g) {
		if (!visible) {
			return;
		}
		super.paint(g);
		if (image != null) {
			if (scaled) {
				g.drawImage(image, src, absBound);
			} else {
				g.drawImage(image, absBound.left, absBound.top, Layout.TOP_LEFT);
			}
		}
	}
	
}
